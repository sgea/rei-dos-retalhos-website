<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Rei dos Retalhos - Artigos e Artefatos de Couro</title>
        <link rel="shortcut icon" type="image/png" href="/img/favicon.ico"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/estilo.css" />
    </head>

  	<body>

      <header>

          <div class="container-fluid">

            <div class="row">

              <nav class="menu col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <ul class="nav nav-pills nav-justified">
                  <li role="presentation">
                    <a href="{{ action("MainController@index") }}">Home</a>
                  </li>
                  <li role="presentation">
                    <a href="{{ action("MainController@localizacao") }}">Localização</a>
                  </li>
                  <li role="presentation">
                    <a href="{{ action("MainController@getContato") }}">Contato</a>
                  </li>
                </ul>
              </nav>

              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h1 id="slogan" class="text-center">
                  <span id="elemento-coroa">Rei dos Retalhos</span>
                </h1>
              </div>

            </div>

          </div>

      </header>

      @yield('conteudo')

      <footer>
        <div class="container-fluid">
          <div class="row">

            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">

              <h3 class="text-center">Rei dos Retalhos</h3>
              <p>
                <address>
                  Avenida: Presidente Vargas, 1295<br/>
                  Cidade Nova - Franca/SP, Brasil
                </address>  
              </p>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
              <h3>Contato</h3>
              <p>
                <span>(16) 3721-6073</span><br/>
                <span>reidosretalhos@gmail.com</span>
              </p>
            </div>

          </div>
        </div>
      </footer>

    </div>

		<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>

  </body>
    
</html>