@extends('master')

@section('conteudo')
	
<div class="container-fluid">

	<h2 class="display-none">Produtos Rei dos Retalhos</h2>

     <div class="row">

        <h3 class="text-center subtitulo">Nobuck</h3>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">

                <a href="#" class="thumbnail">
                    <img src="/img/couro2.jpg" alt="..." title="Nobuck Castor">
                    <div class="img-titulo">
                        <p class="txt-titulo">Nobuck Castor</p>
                    </div>
                </a>
                
            </div> 

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">

                <a href="#" class="thumbnail">
                    <img src="/img/couro1.jpg" alt="..." title="Nobuck Castor">
                    <div class="img-titulo">
                        <p class="txt-titulo">Nobuck Azul Bic</p>
                    </div>
                </a>
                
            </div> 

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">

                <a href="#" class="thumbnail">
                    <img src="/img/couro2.jpg" alt="..." title="Nobuck Castor">
                    <div class="img-titulo">
                        <p class="txt-titulo">Nobuck Vermelho</p>
                    </div>
                </a>
                
            </div> 

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">

                <a href="#" class="thumbnail">
                    <img src="/img/couro1.jpg" alt="..." title="Nobuck Castor">
                    <div class="img-titulo">
                        <p class="txt-titulo">Nobuck Preto</p>
                    </div>
                </a>
                
            </div>     

        </div>

    </div>

    <div class="row">

        <h3 class="text-center subtitulo">Latego</h3>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">

                <a href="#" class="thumbnail">
                    <img src="/img/couro2.jpg" alt="..." title="Nobuck Castor">
                    <div class="img-titulo">
                        <p class="txt-titulo">Nobuck Castor</p>
                    </div>
                </a>
                
            </div> 

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">

                <a href="#" class="thumbnail">
                    <img src="/img/couro1.jpg" alt="..." title="Nobuck Castor">
                    <div class="img-titulo">
                        <p class="txt-titulo">Nobuck Azul Bic</p>
                    </div>
                </a>
                
            </div> 

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">

                <a href="#" class="thumbnail">
                    <img src="/img/couro2.jpg" alt="..." title="Nobuck Castor">
                    <div class="img-titulo">
                        <p class="txt-titulo">Nobuck Vermelho</p>
                    </div>
                </a>
                
            </div> 

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">

                <a href="#" class="thumbnail">
                    <img src="/img/couro1.jpg" alt="..." title="Nobuck Castor">
                    <div class="img-titulo">
                        <p class="txt-titulo">Nobuck Preto</p>
                    </div>
                </a>
                
            </div>     

        </div>

    </div>


    <div class="row">

        <h3 class="text-center subtitulo">Vestuário</h3>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">

                <a href="#" class="thumbnail">
                    <img src="/img/couro2.jpg" alt="..." title="Nobuck Castor">
                    <div class="img-titulo">
                        <p class="txt-titulo">Nobuck Castor</p>
                    </div>
                </a>
                
            </div> 

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">

                <a href="#" class="thumbnail">
                    <img src="/img/couro1.jpg" alt="..." title="Nobuck Castor">
                    <div class="img-titulo">
                        <p class="txt-titulo">Nobuck Azul Bic</p>
                    </div>
                </a>
                
            </div> 

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">

                <a href="#" class="thumbnail">
                    <img src="/img/couro2.jpg" alt="..." title="Nobuck Castor">
                    <div class="img-titulo">
                        <p class="txt-titulo">Nobuck Vermelho</p>
                    </div>
                </a>
                
            </div> 

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">

                <a href="#" class="thumbnail">
                    <img src="/img/couro1.jpg" alt="..." title="Nobuck Castor">
                    <div class="img-titulo">
                        <p class="txt-titulo">Nobuck Preto</p>
                    </div>
                </a>
                
            </div>     

        </div>

    </div>

    <div class="row">

        <h3 class="text-center subtitulo">Napa Flay</h3>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">

                <a href="#" class="thumbnail">
                    <img src="/img/couro2.jpg" alt="..." title="Nobuck Castor">
                    <div class="img-titulo">
                        <p class="txt-titulo">Nobuck Castor</p>
                    </div>
                </a>
                
            </div> 

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">

                <a href="#" class="thumbnail">
                    <img src="/img/couro1.jpg" alt="..." title="Nobuck Castor">
                    <div class="img-titulo">
                        <p class="txt-titulo">Nobuck Azul Bic</p>
                    </div>
                </a>
                
            </div> 

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">

                <a href="#" class="thumbnail">
                    <img src="/img/couro2.jpg" alt="..." title="Nobuck Castor">
                    <div class="img-titulo">
                        <p class="txt-titulo">Nobuck Vermelho</p>
                    </div>
                </a>
                
            </div> 

            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">

                <a href="#" class="thumbnail">
                    <img src="/img/couro1.jpg" alt="..." title="Nobuck Castor">
                    <div class="img-titulo">
                        <p class="txt-titulo">Nobuck Preto</p>
                    </div>
                </a>
                
            </div>     

        </div>

    </div>

    
</div>

@stop