@extends('master')
@section('conteudo')


<div class="container-fluid">

	<div class="row">

		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">

			<h1 id="titulo">Contato</h1>

			@if( $enviado == true)
				<p class="alert alert-success" role="alert">
					Sua mensagem foi enviada com sucesso
				</p>
			@endif

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

				<h2>Dúvida! Nos envie um e-mail</h2>

				{{ Form::open( array('action' => 'MainController@postContato', 'class' => 'form') ) }}
					
					<div class="form-group">
						{{ Form::label('nome', 'Nome*') }}
						<br />
						{{ Form::text('nome') }}
					</div>

					<div class="form-group">
						{{ Form::label('telefone', 'Telefone') }}
						<br />
						{{ Form::text('telefone') }}
					</div>

					<div class="form-group">
						{{ Form::label('email', 'Email*') }}
						<br />
						{{ Form::email('email') }}
					</div>

					<div class="form-group">
						{{ Form::label('mensagem', 'Mensagem') }}
						<br />
						{{ Form::textarea('mensagem') }}
					</div>

					<p style="font-size: 28px;">{{ Form::submit("Enviar") }}</p>

					{!! Form::close() !!}
			
					@if($errors->any())
						@foreach($errors->all() as $error)
							<p class="alert alert-danger" role="alert">{{ $error }}</p>
						@endforeach
					@endif

			</div>

			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">

				<h2>Informe-se!</h2>

				<p>
					Precisa saber algo? Nos contate pelo telefone ou envie um email estaremos mais que felizes em te ajudar.
				</p>

				<p>Telefone: (16) 3721-6073</p>
				<p>Email: reidosretalhos@hotmail.com</p>
				<address>
					Avenida: Presidente Vargas, Nº 1295, Cidade Nova - Franca/SP
				</address>

			</div>

		</div>

	</div>

</div>


@stop