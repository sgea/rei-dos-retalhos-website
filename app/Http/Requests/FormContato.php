<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FormContato extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required',
            'email' => 'required',
            'mensagem' => 'required | min: 10'
        ];
    }

    /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
    public function messages()
    {
        return [
            'nome.required' => 'O campo nome é obrigatório',
            'email.required' => 'O campo e-mail é obrigatório',
            'mensagem.required' => 'O campo mensagem é obrigatório',
            'mensagem.min' => "O campo necessita de no minimo 10 caracteres"
        ];
    }

}
