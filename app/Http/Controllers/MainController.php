<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\FormContato;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    
	public function index()
	{
		return view('index');
	}

	public function produtos()
	{
		return view('produtos');
	}

	public function localizacao()
	{
		return view('localizacao');
	}

	public function getContato()
	{
		$enviado = false;
		return view('contato', compact('enviado'));
	}

	public function postContato(FormContato $request)
	{
		$enviado = true;
		return view('contato', compact('enviado'));
	}

}
